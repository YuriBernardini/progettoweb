<?php
require_once("bootstrap.php");

//Base Template inutilizzati
$templateParams['Titolo'] = "GoldEvent - AGGIORNA";
$templateParams['Icona'] = "img/icona.png";
$templateParams["Nome"] = "account.php";


if(isset($_SESSION['idutente'])){

    if(($_POST['old-psw']!= "") && ($_POST['new-psw']!= "") && ($_POST['repeat-psw']!= "")){

        $cambio = 1;
        $pwd0 = md5($_POST["old-psw"]);
        $pwd1 = md5($_POST["new-psw"]);
        $pwd2 = md5($_POST["repeat-psw"]);
        //se nuove uguali
        if(pwdOk($pwd1, $pwd2)) {

            //se vecchia pwd corrisponde
            $dbPwd = $dbh->getPassword($_SESSION['idutente']);
            $_SESSION['AllarmeInfo'] ="Attenzione! i campi password sono stati aggiornati";

            if($dbPwd[0]['password'] == $pwd0) {

                $dbh->cambiaPassword($_SESSION['idutente'], $pwd0, $pwd1);

            } else {

                $_SESSION['aggiornaDati'] ="Errore! vecchia password non corretta";

            }
        } else {

            $_SESSION['aggiornaDati'] = "Errore! nuove password non corrispondono";
        }

    }
    
    if(($_POST['new-regione']!= NULL) && ($_POST['new-citta']!= NULL) && ($_POST['new-provincia']!= NULL)){
        
        $cambio = 1;

        $regione = $_POST['new-regione'];
        $provincia = $_POST['new-provincia'];
        $citta = $_POST['new-citta'];

        $dbh->cambiaCitta($regione, $provincia, $citta, $_SESSION['idutente']);
                          
        $_SESSION['AllarmeInfo'] = "Attenzione! i campi città sono stati aggiornati! ";
        
    } 

    if(!isset($cambio)) {
        unset($cambio);
        $_SESSION['aggiornaDati'] ="Errore! nessuna azione";
    }

}

header('location:account.php');

?>