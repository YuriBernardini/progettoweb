<?php
require_once 'bootstrap.php';

//Base Template
$templateParams['Titolo'] = "GoldEvent - Acquista";
$templateParams['Icona'] = "img/icona.png";
$templateParams["Nome"] = "singolo-evento.php";

if(isUserLoggedIn()){

    if(isset($_POST['idevento'])) {

        $risultato = $dbh->getEventByIdEventAndIdUser($_POST["idevento"]);

        if(count($risultato)==0){

            $templateParams["Evento"] = null;
        }
        else{

            $templateParams["Evento"] = $risultato[0];
        }
    } else if(isset($_POST['id'])) {

        //aggiungi al carrello
        $ris = $dbh->getEventByIdEvent($_POST["id"]);
        $event = $ris[0];
        
        //controllare numero disponibile
        if($event['bigliettiDisponibili'] >= $_POST['bigliettiacquistabili']) {
           
            //function load COOKIE
            setCookieCarrello($_SESSION['idutente'], $_POST['id'], $_POST['bigliettiacquistabili']);

            $_SESSION['acquistato'] = "Aggiunto al Carrello";
            header('location:eventi.php');
        } else {

            $_SESSION['errore'] = "Biglietti Non Disponibili";
            header('location:eventi.php');
        }
        
    }
    
}
else{
    header("location: account.php");
}

$templateParams["Categoria"] = $dbh->getCategorie();
$templateParams["Provincia"] = $dbh->getProvince();
$templateParams["Citta"] = $dbh->getCitta();
$templateParams["Regioni"] = $dbh->getRegioni();

require 'template/base.php';

?>
   