<?php
require_once("bootstrap.php");

if(isset($_POST["email"]) && isset($_POST["password"])){

    $email = $_POST["email"];
    $hashedPassword =md5($_POST["password"]);
    
    $login_result = $dbh->checkLogin($email, $hashedPassword);
    if(count($login_result)==0){
        $templateParams["Allarme"] = "Errore! email o password non corretti";
    }
    else{
        registerLoggedUser($login_result[0]);
    }
}

if(isset($_POST["new-email"])){
    $email = $_POST["new-email"];
    //tutti altri controlli
    if(isset($_POST["nome"]) && isset($_POST["cognome"]) && isset($_POST["new-psw"]) && isset($_POST["repeat-psw"]) && ($_POST["data"] != "") && isset($_POST["regione"]) && isset($_POST["provincia"]) && isset($_POST["citta"])) {
        //tutti inseriti
        $nome = $_POST["nome"];
        $cognome = $_POST["cognome"];
        $pwd = md5($_POST["new-psw"]);
        $data = date("Y-m-d H:i:s", strtotime($_POST["data"]));
        $regione = $_POST["regione"];
        $provincia = $_POST["provincia"];
        $citta = $_POST["citta"];
        if(isset($_POST["organizzatore"])) {
            $organizzatore = 1;
        } else  {
            $organizzatore = 0;
        }

        if(emailOk($email)) {

            if($dbh->isNewEmail($email)){

                if(pwdOk(md5($_POST["new-psw"]), md5($_POST["repeat-psw"]))) {

                    if(dataOk($_POST["data"])) {
                        //registrazione
                        $dbh->addNewUser($nome, $cognome, $email, $pwd, $data, $regione, $provincia, $citta, $organizzatore);
                        
                        $login_result = $dbh->checkLogin($email, $pwd);    
                        registerLoggedUser($login_result[0]);

                    } else {
                        $templateParams["Allarme"] = "Errore! Inserisci una data di nascita valida ( 14 anni richiesti )";
                    }

                } else {
                    $templateParams["Allarme"] = "Errore! Password diverse";
                }  

            } else {

                $templateParams["Allarme"] = "Errore! email gia in uso";
            }
        } else {

            $templateParams["Allarme"] = "Errore! email non corretta";
        }
    } else {

        $templateParams["Allarme"] = "Errore! Compila tutti i campi";
    }
    
}

if(isset($_POST["recuperaemail"])){
    $email = $_POST["recuperaemail"];
    $recuperaResult = $dbh->isNewEmail($email);
    //if is a new user, user account not found
    if($recuperaResult){
        $templateParams["Allarme"] = "Utente non registrato!";
    }else{
        $templateParams["AllarmeInfo"] = "Email inviata. Recupero password avvenuto correttamente!";
    }
}

if(isUserLoggedIn()){
    $templateParams["Titolo"] = "Gold Event - Account";
    $templateParams['Icona'] = "img/icona.png";
    $templateParams["Nome"] = "account.php";
    $templateParams["Regioni"] = $dbh->getRegioni();
    if(isset($_SESSION['aggiornaDati'])) {        
        $templateParams["Allarme"] = $_SESSION['aggiornaDati'];
        unset($_SESSION['aggiornaDati']);

    }else if(isset($_SESSION['AllarmeInfo'])) {        
        $templateParams["AllarmeInfo"] = $_SESSION['AllarmeInfo'];
        unset($_SESSION['AllarmeInfo']);
    }
    
    if(isset($_GET["formmsg"])){
        $templateParams["formmsg"] = $_GET["formmsg"];
    }
}
else{
    $templateParams["Titolo"] = "Gold Event - Accedi";
    $templateParams['Icona'] = "img/icona.png";
    $templateParams["Nome"] = "account-login.php";
    $templateParams["Regioni"] = $dbh->getRegioni();
    $templateParams["RecuperaPassword"] = "recupera-password.php";
    /*$templateParams["Provincia"] = $dbh->getRegioni($templateParams["Regioni"]);*/
}

require("template/base.php");
?>