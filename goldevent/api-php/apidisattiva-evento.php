<?php

require_once '../bootstrap.php';

if(isset($_POST['id'])) {
    $dbh->disattivaEvento($_POST['id']);

    $eventi = array();
    $eventi["inattivi"] = $dbh->getEventiNonAttivi();
    $eventi["attivi"] = $dbh->getAllEventi();
    
    echo json_encode($eventi);
}

?>