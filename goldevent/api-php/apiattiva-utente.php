<?php

require_once '../bootstrap.php';

if(isset($_POST['id'])) {
    $dbh->attivaAccount($_POST['id']);
    $utenti = array();
    $utenti["inattivi"] = $dbh->getAllInactiveUsers();
    $utenti["attivi"] = $dbh->getAllActiveUsers();
    for($i = 0; $i < count($utenti["attivi"]); $i++){
        $utenti["attivi"][$i]["cognome"]= preg_replace("/(^...)(*SKIP)(*F)|(.)/","*", $utenti["attivi"][$i]["cognome"]);
        $utenti["attivi"][$i]["nome"]= preg_replace("/(^...)(*SKIP)(*F)|(.)/","*", $utenti["attivi"][$i]["nome"]);
        $utenti["attivi"][$i]["email"] =preg_replace('/(?:^|@).\K|\.[^@]*$(*SKIP)(*F)|.(?=.*?\.)/', '*', $utenti["attivi"][$i]["email"]);
    }
    for($j = 0; $j < count($utenti["inattivi"]); $j++){
        $utenti["inattivi"][$j]["cognome"]= preg_replace("/(^...)(*SKIP)(*F)|(.)/","*", $utenti["inattivi"][$j]["cognome"]);
        $utenti["inattivi"][$j]["nome"]= preg_replace("/(^...)(*SKIP)(*F)|(.)/","*", $utenti["inattivi"][$j]["nome"]);
        $utenti["inattivi"][$j]["email"] =preg_replace('/(?:^|@).\K|\.[^@]*$(*SKIP)(*F)|.(?=.*?\.)/', '*', $utenti["inattivi"][$j]["email"]);
    }

    echo json_encode($utenti);
}

?>