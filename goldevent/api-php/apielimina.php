<?php

require_once '../bootstrap.php';

header('Content-Type: application/json');
    if(isset($_POST["action"]))
    {
        $idevento = $_POST["idevento"];
        $utente = $_SESSION["idutente"];
    
        $dbh->deleteEvent($idevento, $utente);
        $msg = "Eliminazione effettuata correttamente!";
    }else{
        $msg = "Eliminazione non effettuata!";
    }
    $templateParams["AllarmeInfo"] = $msg;
    echo json_encode($msg);

?>