<?php
require_once '../bootstrap.php';

header('Content-Type: application/json');
if(isset($_POST["regione"]))
{
    $data = $dbh->getProvince($_POST["regione"]);
    echo json_encode($data);
}else{
    echo json_encode("errore");
}
?>