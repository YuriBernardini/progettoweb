<?php
require_once("bootstrap.php");

//Base Template
$templateParams['Titolo'] = "GoldEvent - Carrello";
$templateParams['Icona'] = "img/icona.png";
$templateParams["Nome"] = "carrello.php";

if(!isset($_SESSION['idutente'])) {

    header('location:account.php');
    
}

require 'template/base.php';

?>