<?php

class DatabaseHelper {

    private $db;

    public function __construct($servername, $username, $password, $dbname){
        $this->db = new mysqli($servername, $username, $password, $dbname);
        if ($this->db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }        
    }

    public function getAllActiveUsers(){
        $query = "SELECT id, nome, cognome, email, citta, privilegio FROM persona WHERE attivo=1 AND privilegio <> 1";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAllInactiveUsers(){
        $query = "SELECT id, nome, cognome, email, citta, privilegio FROM persona WHERE attivo=0";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function checkLogin($email, $password){
        $query = "SELECT id, nome, cognome, email, citta, privilegio FROM persona WHERE attivo=1 AND email = ? AND password = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss',$email, $password);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getPassword($id) {
        $query = "SELECT password FROM persona WHERE attivo=1 AND id = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);

    }

    public function cambiaPassword($id, $pwd0, $pwd1) {
        $query = "UPDATE persona SET password = ? WHERE id = ? AND password = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sis',$pwd1, $id, $pwd0);
        
        return $stmt->execute();
    }

    public function getRegioni() {
        $stmt = $this->db->prepare("SELECT DISTINCT regione FROM citta ORDER BY regione ASC");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getProvince($regione = "%%") {
        $query = "SELECT DISTINCT provincia FROM citta WHERE regione LIKE ? ORDER BY provincia ASC";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $regione);
        $stmt->execute();
        $result = $stmt->get_result();
        
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCitta($provincia = "%%") { //variabile di default che prende tutte le regioni
        $query = "SELECT id, comune FROM citta WHERE provincia LIKE ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $provincia);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventByIdEventAndIdUser($idevento, $iduser = "%%"){
        $query = "SELECT evento.id, titolo, data, informazioni, immagine, bigliettiDisponibili, prezzo, evento.citta, citta.regione, citta.provincia, citta.comune, creatore, tipologia, attivo FROM evento JOIN citta ON evento.citta = citta.id WHERE evento.id = ? AND creatore LIKE ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('is', $idevento, $iduser);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getEventByIdEvent($idevento){
        $query = "SELECT id, titolo, data, informazioni, immagine, bigliettiDisponibili, prezzo, citta, creatore, tipologia, attivo FROM evento WHERE id = ? ";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idevento);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCategorie(){
        $query = "SELECT id, descrizione FROM tipologia ORDER BY descrizione";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function insertEvent($titoloevento, $data, $descrizioneevento, $locandinaevento, $biglietti, $prezzoBiglietto, $regione, $provincia, $citta, $utente, $categoria){
        
        $idcategoria = $this->getIdCategoria($categoria);
        $idcitta = $this->getIdCitta($regione, $provincia, $citta);
        
        $query = "INSERT INTO evento (titolo, data, informazioni, immagine, bigliettiDisponibili, prezzo, citta, creatore, tipologia) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $zero = 0;
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssssidiii',$titoloevento, $data, $descrizioneevento, $locandinaevento, $biglietti, $prezzoBiglietto, $idcitta, $utente, $idcategoria);
        $stmt->execute();
        
        return $stmt->insert_id;
    }

    public function updateEventByCreator($idevento, $titoloevento, $data, $descrizioneevento, $locandinaevento, $biglietti, $prezzoBiglietto, $regione, $provincia, $citta, $utente, $categoria){

        $idcategoria = $this->getIdCategoria($categoria);
        $idcitta = $this->getIdCitta($regione, $provincia, $citta);
        
        $query = "UPDATE evento SET titolo = ?, data= ?, informazioni = ?, immagine = ?, bigliettiDisponibili = ?, prezzo = ?, citta = ?, tipologia= ? WHERE id = ? AND creatore = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssssidiiii',$titoloevento, $data, $descrizioneevento, $locandinaevento, $biglietti, $prezzoBiglietto, $idcitta, $idcategoria, $idevento, $utente);
        return $stmt->execute();
    }

    public function deleteEvent($idevento, $utente){
        $query = "UPDATE evento SET attivo = 0 WHERE id = ? AND creatore = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii',$idevento, $utente);
        return $stmt->execute();
    }


    public function getUserEvents($utente = "%%"){
        $query = "SELECT evento.id, titolo, data, informazioni, immagine, prezzo, bigliettiDisponibili, evento.citta, creatore, tipologia, tipologia.descrizione, citta.regione, citta.provincia, citta.comune, attivo FROM evento JOIN citta ON evento.citta = citta.id JOIN tipologia ON evento.tipologia = tipologia.id WHERE creatore LIKE ? AND attivo = 1";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $utente);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAllEventi(){
        $query = "SELECT evento.id, titolo, data, informazioni, prezzo, immagine, bigliettiDisponibili, evento.citta, creatore, tipologia, tipologia.descrizione, citta.regione, citta.provincia, citta.comune, attivo FROM evento JOIN citta ON evento.citta = citta.id JOIN tipologia ON evento.tipologia = tipologia.id WHERE attivo = 1";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAllEventiNonSuperati(){
        $query = "SELECT evento.id, titolo, data, informazioni, prezzo, immagine, bigliettiDisponibili, evento.citta, creatore, tipologia, tipologia.descrizione, citta.regione, citta.provincia, citta.comune, attivo FROM evento JOIN citta ON evento.citta = citta.id JOIN tipologia ON evento.tipologia = tipologia.id WHERE attivo = 1 AND data >= NOW()";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getEventiNonAttivi(){
        $query = "SELECT id, titolo, data, prezzo, informazioni, immagine, bigliettiDisponibili, citta, creatore, tipologia, attivo FROM evento WHERE attivo = 0";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function isNewEmail($email) {
        $query = "SELECT * FROM persona WHERE attivo=1 AND email = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$email);
        $stmt->execute();
        $result = $stmt->get_result();

        $emailResult = $result->fetch_all(MYSQLI_ASSOC);

        if(count($emailResult)==0) {
            return true;
        } else {
            return false;
        }  
    }

    public function addNewUser($nome, $cognome, $email, $pwd, $data, $regione, $provincia, $citta, $organizzatore) {
        
        $idcitta = $this->getIdCitta($regione, $provincia, $citta);

        if($organizzatore == 1) {
            $idprivilegio = 2;
        } else {
            $idprivilegio = 3;
        }
        $att=1;
        $query = "INSERT INTO persona (nome, cognome, email, password, dataDiNascita, attivo, citta, privilegio) VALUES ( ?, ?, ?, ?, ?, 1, $idcitta, $idprivilegio)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sssss', $nome, $cognome, $email, $pwd, $data);
        $stmt->execute();

    }

    public function cambiaCitta($regione, $provincia, $citta, $id) {

        $idcitta = $this->getIdCitta($regione, $provincia, $citta);
        
        $query = "UPDATE persona SET citta = ? WHERE id = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii',$idcitta, $id);
        
        return $stmt->execute();

    }

    public function attivaAccount($id){
        $query = "UPDATE persona SET attivo = 1 WHERE id = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$id);
        
        return $stmt->execute(); 
    }

    public function disattivaAccount($id){
        $query = "UPDATE persona SET attivo = 0 WHERE id = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$id);
        
        return $stmt->execute(); 
    }
    
    public function attivaEvento($id){
        $query = "UPDATE evento SET attivo = 1 WHERE id = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$id);
        
        return $stmt->execute(); 
    }

    public function disattivaEvento($id){
        $query = "UPDATE evento SET attivo = 0 WHERE id = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$id);
        
        return $stmt->execute(); 
    }

    public function getCittaSelezionata($idcitta){
        $query = "SELECT id, comune, provincia, regione FROM citta WHERE id = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $idcitta);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getBiglietti($idutente) {

        $query = "SELECT evento.id, evento.titolo, evento.data, biglietto.quantita, biglietto.totale FROM `biglietto` JOIN evento ON evento.id = biglietto.evento WHERE biglietto.persona = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $idutente);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getBigliettoByIds($idutente, $idevento) {

        $query = "SELECT evento.titolo, evento.data, biglietto.quantita, biglietto.totale FROM `biglietto` JOIN evento ON evento.id = biglietto.evento WHERE biglietto.persona = ? AND evento.attivo = 1 AND evento.id = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ii", $idutente, $idevento);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function buyFirstTicket($idutente, $idevento, $quantita, $totale){
        
        $this->deleteticket($idevento, $quantita);

        $query = "INSERT INTO `biglietto` (`persona`, `evento`, `quantita`, `totale`) VALUES (?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('iiid',$idutente, $idevento, $quantita, $totale);
        $stmt->execute();
        
        return $stmt->insert_id;
    }

    public function buyOtherTicket($idutente, $idevento, $quantita, $totale) {
        
        $this->deleteticket($idevento, $quantita);

        $ticket = $this->getTotalByTicket($idutente, $idevento);
        $tot = $ticket[0];

        //prezzo totale + prezzo nuovo, quantita totale + quantita nuova
        $nuovoTotale = $tot['quantita'] + $quantita;
        $nuovoSpesa = $tot['totale'] + $totale;

        $query = "UPDATE `biglietto` SET `quantita` = $nuovoTotale, `totale` = $nuovoSpesa WHERE `biglietto`.`persona` = ? AND `biglietto`.`evento` = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $idutente, $idevento);
        
        return $stmt->execute(); 
    }

    private function getTotalByTicket($idutente, $idevento) {

        $query = "SELECT quantita, totale FROM `biglietto` WHERE persona = ? AND evento= ?";

        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ii", $idutente, $idevento);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    private function deleteticket($idevento, $quantita) {
        
        $query = "UPDATE evento SET bigliettiDisponibili = bigliettiDisponibili - ? WHERE id = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $quantita, $idevento);
        
        return $stmt->execute(); 
    }

    public function getThreeRandomEvents() {
        $query = "SELECT evento.id, evento.titolo, evento.data, evento.prezzo, evento.immagine, citta.comune, citta.provincia, citta.regione FROM evento JOIN citta ON evento.citta = citta.id WHERE evento.attivo = 1 AND evento.data >= NOW() ORDER BY RAND() LIMIT 3";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getMaggiorAcquirente() {
        $query = " SELECT SUM(quantita) as quantita, persona.nome, persona.cognome, persona.email FROM `biglietto` JOIN persona ON persona.id = biglietto.persona GROUP BY persona ORDER BY quantita DESC LIMIT 1";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getMaggiorCitta() {
        $query = " SELECT COUNT(*) as numero, citta.comune, citta.regione, citta.provincia FROM evento JOIN citta ON citta.id = evento.citta GROUP BY citta.id ORDER BY numero DESC LIMIT 1";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

   private function getIdCategoria($categoria) {
        $query = "SELECT id FROM tipologia WHERE descrizione = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $categoria);
        $stmt->execute();
        $result = $stmt->get_result();

        $id = $result->fetch_all(MYSQLI_ASSOC);
        return $id[0]['id']; 
    }
    
    private function getIdCitta($regione, $provincia, $citta) {
        $query = "SELECT id FROM `citta` WHERE regione=? AND provincia=? AND comune=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sss',$regione, $provincia, $citta);
        $stmt->execute();
        $result = $stmt->get_result();

        $id = $result->fetch_all(MYSQLI_ASSOC);
        return $id[0]['id']; 
    }
}

?>