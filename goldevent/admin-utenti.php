<?php

require_once("bootstrap.php");

if(isset($_SESSION['idutente']) && $_SESSION['privilegio'] == 1) {
    //Base Template
    $templateParams['Titolo'] = "GoldEvent - Admin";
    $templateParams['Icona'] = "img/icona.png";
    $templateParams["Nome"] = "admin-utenti.php";
    
    $templateParams["UtentiAttivi"] = $dbh->getAllActiveUsers();
    $templateParams["UtentiNonAttivi"] = $dbh->getAllInactiveUsers();
    
    require 'template/base.php';

} else {
   header('location:index.php');
}

?>