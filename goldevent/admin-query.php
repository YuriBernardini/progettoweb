<?php

require_once("bootstrap.php");

if(isset($_SESSION['idutente']) && $_SESSION['privilegio'] == 1) {
    //Base Template
    $templateParams['Titolo'] = "GoldEvent - Admin";
    $templateParams['Icona'] = "img/icona.png";
    $templateParams["Nome"] = "admin-query.php";
    
    $templateParams["MigliorAcquirente"] = $dbh->getMaggiorAcquirente();
    $templateParams["MigliorCitta"] = $dbh->getMaggiorCitta();
    
    require 'template/base.php';

} else {
   header('location:index.php');
}

?>