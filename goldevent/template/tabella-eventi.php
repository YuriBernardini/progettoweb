                <?php 
                    if(isset($templateParams["Eventi"])){
                        $eventi = $templateParams["Eventi"];
                    }
                ?>
                <div class="col-lg px-0">
                    <form action="<?php echo $templateParams["Home-Evento"];?>" method="POST" id="click-form" enctype="multipart/form-data">
                    <table class="table user-table table-hover" id="myTable">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" hidden>Id</th>
                                <th scope="col" hidden>Categoria</th>
                                <th scope="col">Titolo</th>
                                <th scope="col" class="hide-column">Data</th>
                                <th scope="col">Prezzo</th>
                                <th scope="col" class="hide-column">Biglietti disponibili</th>
                                <th scope="col" hidden>Regione</th>
                                <th scope="col" hidden>Provincia</th>
                                <th scope="col" hidden>Citt&agrave;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($eventi as $evento) :?>
                                <tr class="row-table clickable-row">
                                        <td class="align-middle idevento" hidden><?php echo $evento["id"]; ?></td>
                                        <td class="align-middle" hidden><?php echo $evento["descrizione"]; ?></td>
                                        <td class="align-middle"><?php echo $evento["titolo"]; ?></td>
                                        <td class="hide-column align-middle"><?php echo $evento["data"]; ?></td>
                                        <td class="align-middle"><?php echo $evento["prezzo"]; ?> &#8364;</td>
                                        <td class="hide-column align-middle"><?php echo $evento["bigliettiDisponibili"]; ?></td>
                                        <td class="align-middle" hidden><?php echo $evento["regione"]; ?></td>
                                        <td class="align-middle" hidden><?php echo $evento["provincia"]; ?></td>
                                        <td class="align-middle" hidden><?php echo $evento["comune"]; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <?php if(isset($templateParams["Azione"])): ?>
                    <input type="hidden" name="action" value="<?php echo $templateParams["Azione"]; ?>" class ="action"/>
                    <?php endif ?>
                    <input type="hidden" name="idevento" value="" class ="idevento-form"/>
                    </form>
                </div>