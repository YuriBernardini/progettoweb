<div class="con">
    <div class="row">
        <div class="col-md rounded form-group mx-3 event-form">
                <h2 class='my-4'>EVENTI NON ATTIVI</h2>
                <div class='row'>
                <div class='col-lg-12'>
                        <table class='table user-table'>
                            <thead class='thead-light'>
                                <tr>
                                    <th scope='col'>Id</th>
                                    <th scope='col'>Titolo</th>
                                    <th scope='col' class='hide-column'>Data</th>
                                    <th scope='col'></th>
                                </tr>
                            </thead>
                            <tbody class="eventiNonAttiviTbody">
                            <?php foreach($templateParams["EventiNonAttivi"] as $evento) :
                                echo "<tr class='row-table attivaEvento'>
                                        <td class='align-middle id-evento-disattivato'>".$evento["id"]."</td>
                                        <td class='align-middle'>".$evento["titolo"]."</td>
                                        <td class='align-middle hide-column'>".$evento["data"]."</td>
                                        <td class='align-middle'><button type='submit' class='btn' >Attiva</button></td>
                                    </tr>";
                            endforeach;?>
                        </tbody>
                        </table>
                    </div>
                </div>            
        </div>
    </div>
</div>
<div class="con">
    <div class="row">
        <div class="col-md rounded form-group mx-3 event-form">
                <h2 class='my-4'>EVENTI ATTIVI</h2>
                <div class='row'>
                <div class='col-lg-12'>
                        <table class='table' id="disattivaTable">
                            <thead class='thead-light'>
                                <tr>
                                    <th scope='col'>Id</th>
                                    <th scope='col'>Titolo</th>
                                    <th scope='col' class="hide-column">Data</th>
                                    <th scope='col'></th>
                                </tr>
                            </thead>
                            <tbody class="eventiAttiviTbody">
                            <?php foreach($templateParams["EventiAttivi"] as $evento) :
                                echo "<tr class='row-table disattivaEvento'>
                                        <td class='align-middle id-evento-attivato' >".$evento["id"]."</td>
                                        <td class='align-middle'>".$evento["titolo"]."</td>
                                        <td class='align-middle hide-column'>".$evento["data"]."</td>
                                        <td class='align-middle'><input type='submit' class='btn' value='Disattiva'/></td>
                                    </tr>";
                            endforeach;?>
                        </tbody>
                        </table>
                    </div>
                </div>            
        </div>
    </div>
</div>