
<div class="con">
    <div class="row">
        <div class="col-md rounded form-group mx-3 event-form">
            <?php if($templateParams["Biglietti"]==NULL):?>
                <h2 class='my-4'>NESSUN ACQUISTO</h2>
            <?php else: ?>
                <h2 class='my-4'>I TUOI EVENTI - ACQUISTI</h2>
                <div class='row'>
                    <div class='col-lg-12'>
                        <form action="<?php echo $templateParams["Home-Evento"];?>" method="POST" id="click-form-biglietti" enctype="multipart/form-data">
                            <table class='table user-table table-hover'>
                                <thead class='thead-light'>
                                    <tr>
                                        <th scope='col'>Titolo</th>
                                        <th scope='col' class='hide-column'>Data</th>
                                        <th scope='col'>Biglietti Acquistati</th>
                                        <th scope='col'>Totale</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($templateParams["Biglietti"] as $evento) :?>
                                        <tr class="row-table clickable-biglietti-eventi" id="<?php echo $evento["id"];?>">
                                            <td class='align-middle'><?php echo $evento["titolo"];?></td>
                                            <td class='align-middle hide-column'><?php echo $evento["data"];?></td>
                                            <td class='align-middle'><?php echo $evento["quantita"];?></td>
                                            <td class='align-middle'><?php echo $evento["totale"];?> &#8364;</td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <input type="hidden" name="idevento" value="" class ="idevento-biglietti-form"/>
                        </form>
                    </div>
                </div>
            <?php endif; ?>            
        </div>
    </div>
</div>

        