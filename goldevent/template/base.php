<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php echo $templateParams["Titolo"]; ?></title>
    <link rel="icon" href="<?php echo $templateParams['Icona']; ?>" type="image/png" />
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="js/function.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" /><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="./css/style.css" />
    <link rel="stylesheet" type="text/css" href="./css/cookiealert.css" />
    <link rel="stylesheet" type="text/css" href="./css/sitealert.css" />
    <link rel="stylesheet" type="text/css" href="./css/sitealertinfo.css" />
</head>
<body>
    <?php include "cookiesalert.php" ?>
    <script src="js/cookiealert.js"></script>
    <header>
        <?php include "sitealert.php" ?>
        <script src="js/sitealert.js"></script>
        <?php include "sitealertinfo.php" ?>
        <script src="js/sitealertinfo.js"></script>
    </header>
    <nav class="navbar navbar-expand-md navbar-light bg-light">
        <a href="index.php" class="navbar-brand"><img src="./img/logo.png" alt="gold event" class="img-fluid logo"/></a>
        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li <?php isActive("index.php");?> >
                    <a class="nav-link" href="index.php">HOME</a>
                </li>
                <li <?php isActive("eventi.php");?>>
                    <a class="nav-link" href="eventi.php">EVENTI</a>
                </li>
                <li <?php isActive("biglietti.php");?>>
                    <a class="nav-link" href="biglietti.php">BIGLIETTI</a>
                </li>
                <?php 
                    if(isset($_SESSION['idutente']) && $_SESSION['privilegio'] == 1) {
                        isActiveAdmin('admin.php');
                    }
                ?>

                <!-- <li <?php isActive("gestisci-eventi.php");?>>
                    <a class="nav-link" href="gestisci-eventi.php">GESTISCI EVENTI</a>
                </li> -->
            </ul>
            <ul class="navbar-nav ml-auto mw-100 icon-menu"> 
                <li <?php isActive("account.php");?>>
                    <a class="nav-link" href="account.php"><img class="user-icon" src="<?php echo UPLOAD_DIR."userIcon_2.png" ?>" alt="user icon"/><span class="text-icon">UTENTE</span></a>
                </li>
                <li <?php isActive("carrello.php");?>>
                    <a class="nav-link" href="carrello.php"><img src="<?php echo UPLOAD_DIR."shoppingBasketIcon.png" ?>" alt="shopping basket icon"/><span class="text-icon">CARRELLO</span></a>
                </li>
            </ul>
        </div>
    </nav>
    <main class="mx-3"> 
        <?php
        if(isset($templateParams["Nome"])){
            require($templateParams["Nome"]);
        }
        ?>
    </main>
    <footer class="mx-3">
        <p class="font-weight-bold">Gold Event</p><p><a href="mailto:yuri@bernardini@studio.unibo.it">Bernardini Yuri</a> & <a href="mailto:anthony.guglielmi@studio.unibo.it">Guglielmi Anthony</a></p><p>A.A. 2019/2020</p>
        <a href="https://www.facebook.com"><img src="./img/facebook.png" alt="go to facebook" class="mr-3"/></a>
        <a href="https://www.youtube.com"><img src="./img/youtube.png" alt="go to youtube" class="mr-3"/></a>
        <a href="https://www.google.com"><img src="./img/google.png" alt="go to google" class="mr-3"/></a>
    </footer>
</body>
</html>