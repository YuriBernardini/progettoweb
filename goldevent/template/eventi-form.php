<?php 
    $evento = $templateParams["Evento"]; 
    $azione = getAction($templateParams["Azione"]);
?>
<div class="con">
    <div class="row">
        <div class="col-md rounded form-group mx-3 event-form">
            <form action="processa-evento.php" method="POST" enctype="multipart/form-data">
                <h2 class="my-4"><?php if($templateParams["Azione"]!= 1): ?>
                GESTISCI EVENTO
                <?php else: ?>
                INSERISCI EVENTO
                <?php endif; ?>
                </h2>
                <?php if($evento == null): ?>
                    <p>Evento non trovato!</p>
                <?php else: ?>
                    <div class="form-group row">
                        <label for="titoloevento" class="col-lg-2 col-form-label">Titolo </label>
                        <div class="col-lg-10">
                            <input type="text" id="titoloevento" name="titoloevento" value="<?php echo $evento["titolo"]; ?>" class="form-control" required/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="descrizioneevento" class="col-lg-2 col-form-label">Descrizione </label>
                        <div class="col-lg-10">
                            <textarea rows="10" id="descrizioneevento" name="descrizioneevento" class="form-control" required><?php echo $evento["informazioni"]; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="categoria" class="col-lg-2 col-form-label">Categoria </label>
                        <div class="col-lg-10">
                            <select class="form-control col-lg-4 categoriaselection" id="categoria" name="categoria" required>
                                <?php if($templateParams["Azione"] == 1):?>
                                    <option disabled selected value> -- seleziona un'opzione -- </option>
                                <?php else: ?>
                                    <option disabled value> -- seleziona un'opzione -- </option>
                                <?php endif;?>
                                <?php foreach($templateParams["Categoria"] as $categoria): ?>
                                <?php if($templateParams["Azione"] != 1 && $categoria["id"] == $evento["tipologia"]): ?>
                                    <option value="<?php echo $categoria["descrizione"]; ?>" selected><?php echo $categoria["descrizione"]; ?></option>
                                <?php else: ?>
                                    <option value="<?php echo $categoria["descrizione"]; ?>"><?php echo $categoria["descrizione"]; ?></option>
                                <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="regione" class="col-lg-2 col-form-label">Regione </label>
                        <div class="col-lg-10">
                            <select class="form-control col-lg-4 regioneselection" id="regione" name="regione" required>
                            <?php if($templateParams["Azione"] == 1):?>
                                <option disabled selected value> -- seleziona un'opzione -- </option>
                            <?php else: ?>
                                <option disabled value> -- seleziona un'opzione -- </option>
                            <?php endif;?>
                            <?php foreach($templateParams["Regioni"] as $regione): ?>
                            <option value="<?php echo $regione["regione"]; ?>"><?php echo $regione["regione"]; ?></option>
                            <?php endforeach; ?>
                            <?php if(isset($templateParams["CittaSelezionata"])):?>
                                <option value="<?php echo $templateParams["CittaSelezionata"][0]["regione"]; ?>" selected><?php echo $templateParams["CittaSelezionata"][0]["regione"]; ?></option>
                            <?php endif ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="provincia" class="col-lg-2 col-form-label">Provincia </label>
                        <div class="col-lg-10">
                            <select class="form-control col-lg-4 provinciaselection" id="provincia" name="provincia" required>
                            <?php if($templateParams["Azione"] == 1):?>
                                <option disabled selected value> -- seleziona un'opzione -- </option>
                            <?php else: ?>
                                <option disabled value> -- seleziona un'opzione -- </option>
                            <?php endif;?>
                            <?php if(isset($templateParams["CittaSelezionata"])):?>
                                <option value="<?php echo $templateParams["CittaSelezionata"][0]["provincia"]; ?>" selected><?php echo $templateParams["CittaSelezionata"][0]["provincia"]; ?></option>
                            <?php endif ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="citta" class="col-lg-2 col-form-label">Citt&agrave; </label>
                        <div class="col-lg-10">
                            <select class="form-control col-lg-4 cittaselection" id="citta" name="citta" required>
                            <?php if($templateParams["Azione"] == 1):?>
                                <option disabled selected value> -- seleziona un'opzione -- </option>
                            <?php else: ?>
                                <option disabled value> -- seleziona un'opzione -- </option>
                            <?php endif;?>
                            <?php if(isset($templateParams["CittaSelezionata"])):?>
                                <option value="<?php echo $templateParams["CittaSelezionata"][0]["comune"]; ?>" selected><?php echo $templateParams["CittaSelezionata"][0]["comune"]; ?></option>
                            <?php endif ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php if($templateParams["Azione"]!=1): ?>
                            <div class="col-lg">
                                <img src="<?php echo UPLOAD_DIR.$evento["immagine"]; ?>" alt="locandina caricata" class="img-locandina center" />
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="form-group row">
                        <label for="immagine" class="col-lg-2 col-from-label"><?php echo $azione;?> locandina</label>
                        <div class="col-lg-2">
                            <input type="file" class="selectimage" id="immagine" name="immagine" <?php if($templateParams["Azione"]==1){ echo "required";} ?>/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="bigliettidisponibili" class="col-lg-2 col-form-label">Biglietti max </label>
                        <div class="col-lg-1">
                            <input type="number" min="1" id="bigliettidisponibili" name="bigliettidisponibili" value="<?php if($templateParams["Azione"]!=1){
                                echo $evento["bigliettiDisponibili"];
                            }else{
                            $defaultValue = 1;
                            echo $defaultValue;}
                         ?>" required/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="prezzobiglietto" class="col-lg-2 col-form-label">Prezzo unitario € </label>
                        <div class="col-lg-1">
                            <input type="number" min = "0.00" step="0.01" id="prezzobiglietto" name="prezzobiglietto" value="<?php if($templateParams["Azione"]!=1){
                                echo number_format((double)$evento["prezzo"], 2, '.', '');
                            }else{
                                $defaultValue = 0;
                               echo number_format((double)$defaultValue, 2, '.', '');
                            }
                             ?>" required/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="dataevento" class="col-lg-2 col-form-label">Data evento </label>
                        <div class="col-lg-1">
                        <?php if($templateParams["Azione"]!=1):?>
                            <input type="datetime-local" id="dataevento" name="dataevento" value="<?php echo date('Y-m-d\TH:i', strtotime($evento["data"])); ?>" required/>
                        <?php else: ?>
                        <?php 
                            $date = new DateTime(); // Date object using current date and time
                            $currentTimeLocal = $date->format('Y-m-d\TH:i:s');
                        ?>
                            <input type="datetime-local" id="dataevento" name="dataevento" value="<?php echo $currentTimeLocal;?>" min="<?php echo $currentTimeLocal;?>" required/>
                        <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg">
                            <input type="submit" class="btn btn-evento" name="submit" value= "<?php echo $azione; ?>" />
                            <?php if($templateParams["Azione"]!=1): ?>
                                <button type="button" class="btn btn-evento" data-toggle="modal" data-target="#exampleModal">
                                Elimina
                                </button>
                                    <!-- Modal -->
                                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <h3 class="modal-title" id="exampleModalLabel">Vuoi davvero eliminare l'evento?</h3>
                                        </div>
                                        <div class="modal-footer border-0">
                                            <button type="button" class="btn delete-event-button" data-dismiss="modal">Elimina</button>
                                            <button type="button" class="btn" data-dismiss="modal">Annulla</button>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                <input type="hidden" name="idevento" value="<?php echo $evento["id"]; ?>" class ="idevento"/>
                                <input type="hidden" name="locandinavecchia" value="<?php echo $evento["immagine"]; ?>" />
                                <input type="hidden" name="idutente" value="<?php echo $evento["creatore"]; ?>" />
                            <?php endif;?>
                            <a href="cruscotto-eventi.php" class="btn btn-evento">Annulla</a>
                            <input type="hidden" name="action" value="<?php echo $templateParams["Azione"]; ?>" />
                        </div>
                    </div>
                <?php endif; ?>
            </form>
        </div>
    </div>
</div>