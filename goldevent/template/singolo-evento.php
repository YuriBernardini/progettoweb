<?php
    $evento = $templateParams["Evento"];
?>

<div class="square">
    <h2 class="py-2">ACQUISTA EVENTO</h2>
</div>
<div class="con">
    <div class="row">
        <div class="col-md form-group mx-3 pb-5 event-form">
                <?php   $date = new DateTime(); // Date object using current date and time
                        $currentTimeLocal = $date->format('Y-m-d\ H:i:s');
                ?>
                <?php if(($evento["attivo"] == 0) && $evento["data"] < $currentTimeLocal):?>
                        <p class="mt-5"><strong>Evento non pi&ugrave; disponibile!</strong></p>
                <?php elseif(($evento["attivo"] == 0) && $evento["data"] > $currentTimeLocal):?>
                        <p class="mt-5"><strong>Evento annullato dall'organizzatore o dagli amministratori!</strong></p>                    
                        <p><strong class="mt-3">L'importo verr&agrave; rimborsato nella carta associata al pagamento.</strong></p>
                <?php elseif($evento == null): ?>
                    <p>Evento non trovato!</p>
                <?php else: ?>
                    <form method="POST">
                        <input type="hidden" name="id" value="<?php echo $evento['id']?>" /><br/>
                        <h3 class="text-center"><?php echo $evento["titolo"]; ?></h3>
                        <div class="col-lg">
                            <img src="<?php echo UPLOAD_DIR.$evento["immagine"]; ?>" alt="locandina caricata" class="img-locandina center my-5" />
                        </div>
                        <strong>INFO</strong>
                        <p class="text-left mt-3 mx-2 text-justify"><?php echo nl2br($evento['informazioni']);?></p><br/>
                        <strong>DATA E ORA</strong>
                        <p><?php echo $evento['data']?></p><br/>
                        <strong>PREZZO SINGOLO BIGLIETTO</strong>
                        <p><?php echo $evento['prezzo']?> &#8364;</p><br/>
                        <strong>BIGLIETTI RIMASTI</strong>
                        <p><?php echo $evento['bigliettiDisponibili']?></p><br/>
                            <div class="acquisto rounded my-1 px-5 py-3">
                                <?php if($evento["data"] < $currentTimeLocal):?>
                                    <strong>Evento non pi&ugrave; acquistabile!</strong>
                                <?php else:?>
                                <div class="nbiglietti py-3 m-2">
                                    <label for="bigliettiacquistabili" class="mb-4"> Seleziona quanti biglietti vuoi acquistare (MAX 4 alla volta)</label><br/>
                                    <input type="number" min="1" max="4" id="bigliettiacquistabili" name="bigliettiacquistabili" value="1" required/><br/>
                                </div>
                                <input type="submit" name="ciao" class="mt-5 btn" id = "ciao" value="Aggiungi al carrello" />
                                <?php endif;?>
                            </div>
                    </form>
                <?php endif; ?>
        </div>
    </div>
</div>