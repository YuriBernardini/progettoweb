<div class="con">
    <div class="row">
        <div class="col-md rounded form-group mx-3 event-form">
                <h2 class='my-4'>UTENTI NON ATTIVI</h2>
                <div class='row'>
                <div class='col-lg-12'>
                        <table class='table user-table'>
                            <thead class='thead-light'>
                                <tr>
                                    <th scope='col' class='hide-column'>Id</th>
                                    <th scope='col' class='hide-column'>Cognome</th>
                                    <th scope='col' class='hide-column'>Nome</th>
                                    <th scope='col'>Email</th>
                                    <th scope='col'></th>
                                </tr>
                            </thead>
                            <tbody class="utentiNonAttiviTbody">
                            <?php foreach($templateParams["UtentiNonAttivi"] as $utente) :?>
                                    <tr class='row-table attivaUtente'>
                                        <td class='align-middle hide-column id-utente-disattivato'><?php echo $utente["id"];?></td>
                                        <td class='align-middle hide-column'><?php echo preg_replace("/(^...)(*SKIP)(*F)|(.)/","*", $utente["cognome"]);?></td>
                                        <td class='align-middle hide-column'><?php echo preg_replace("/(^...)(*SKIP)(*F)|(.)/","*", $utente["nome"]);?></td>
                                        <td class='align-middle'><?php echo preg_replace('/(?:^|@).\K|\.[^@]*$(*SKIP)(*F)|.(?=.*?\.)/', '*', $utente["email"]);?></td>
                                        <td class='align-middle'><button type='submit' class='btn' >Attiva</button></td>
                                    </tr>
                            <?php endforeach;?>
                        </tbody>
                        </table>
                    </div>
                </div>            
        </div>
    </div>
</div>
<div class="con">
    <div class="row">
        <div class="col-md rounded form-group mx-3 event-form">
                <h2 class='my-4'>UTENTI ATTIVI</h2>
                <div class='row'>
                <div class='col-lg-12'>
                        <table class='table' id="disattivaTable">
                            <thead class='thead-light'>
                                <tr>
                                    <th scope='col' class='hide-column'>Id</th>
                                    <th scope='col' class='hide-column'>Cognome</th>
                                    <th scope='col' class='hide-column'>Nome</th>
                                    <th scope='col'>Email</th>
                                    <th scope='col'></th>
                                </tr>
                            </thead>
                            <tbody class="utentiAttiviTbody">
                            <?php foreach($templateParams["UtentiAttivi"] as $utente) :?>
                                    <tr class='row-table disattivaUtente'>
                                        <td class='align-middle hide-column id-utente-attivato'><?php echo $utente["id"];?></td>
                                        <td class='align-middle hide-column'><?php echo preg_replace("/(^...)(*SKIP)(*F)|(.)/","*", $utente["cognome"]);?></td>
                                        <td class='align-middle hide-column'><?php echo preg_replace("/(^...)(*SKIP)(*F)|(.)/","*", $utente["nome"]);?></td>
                                        <td class='align-middle'><?php echo preg_replace('/(?:^|@).\K|\.[^@]*$(*SKIP)(*F)|.(?=.*?\.)/', '*', $utente["email"]);?></td>
                                        <td class='align-middle'><input type='submit' class='btn' value='Disattiva'/></td>
                                    </tr>
                            <?php endforeach;?>
                        </tbody>
                        </table>
                    </div>
                </div>            
        </div>
    </div>
</div>