<div class="con">
    <div class="row">
        <div class="col-md rounded form-group mx-3 event-form">
            <h2 class="my-4">SCEGLI L'EVENTO CHE FA PER TE!</h2>
            <div class="row">
                <div class="navbar-size">
                    <div class="col-11 col-sm-12 col-md-11 col-lg-11 col-xl-11 rounded form-group mx-3 event-form navbar-eventi">
                        <nav class="navbar rounded navbar-expand-lg navbar-expand-md navbar-light bg-light px-0">
                            <button type="button" class="navbar-toggler rounded-lg ml-3" data-toggle="collapse" data-target="#navbar2" aria-controls="navbar2" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="fa fa-bars mr-2"></span><small class="font-weight-bold">Filtro</small>
                            </button>
                            <div class="navbar-collapse collapse" id="navbar2">
                                <ul class="navbar-nav flex-column text-center">
                                <li class="nav-item active text-center">
                                    <label for="categoria" class="col-md-3 col-lg-3 col-form-label ml-0 pl-0">Categoria </label>
                                    <div class="col-lg">
                                        <select class="form-control col-lg categoriaselection" id="categoria" onchange="filterTableCategoria()" name="categoria">
                                            <option disabled selected value>-- seleziona un'opzione --</option>
                                            <?php foreach($templateParams["Categoria"] as $categoria): ?>
                                            <option value="<?php echo $categoria["descrizione"]; ?>"><?php echo $categoria["descrizione"]; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                </div>
                                </li>
                                <li class="nav-item active text-center">
                                        <label for="regione" class="col-md-3 col-lg-3 col-form-label ml-0 pl-0">Regione </label>
                                        <div class="col-lg">
                                            <select class="form-control col-lg regioneselection" id="regione" onchange="filterTableRegione()" name="regione">
                                            <option disabled selected value>-- seleziona un'opzione --</option>
                                            <?php foreach($templateParams["Regioni"] as $regione): ?>
                                            <option value="<?php echo $regione["regione"]; ?>"><?php echo $regione["regione"]; ?></option>
                                            <?php endforeach; ?>
                                            </select>
                                        </div>
                                </li>
                                <li class="nav-item active text-center">
                                        <label for="provincia" class="col-md-3 col-lg-3 col-form-label ml-0 pl-0">Provincia </label>
                                        <div class="col-lg">
                                            <select class="form-control col-lg provinciaselection" id="provincia" onchange="filterTableProvincia()" name="provincia">
                                            <option disabled selected value>-- seleziona un'opzione --</option>
                                            </select>
                                        </div>
                                </li>
                                <li class="nav-item active text-center">
                                        <label for="citta" class="col-md-3 col-lg-2 col-form-label ml-0 pl-0">Citt&agrave; </label>
                                        <div class="col-lg">
                                            <select class="form-control col-lg cittaselection" id="citta" onchange="filterTableCitta()" name="citta">
                                            <option disabled selected value>-- seleziona un'opzione --</option>
                                            </select>
                                        </div>
                                </li>
                                <li class="nav-item active text-center">
                                        <label for="titoloevento" class="col-md-3 col-lg-3 col-form-label ml-0 pl-0">Titolo </label>
                                        <div class="col-lg">
                                            <input class="col-lg"  type="text" id="titoloevento" onkeyup="filterTableTitolo()" name="titoloevento" placeholder="Titolo" aria-label="Ricerca Titolo">
                                        </div>
                                </li>
                                <li class="nav-item active mt-3">
                                        <input type="button" class="btn btn-resetta-filtro my-2" value="Resetta"/>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
                <div class="col-md rounded form-group mx-3 event-form py-0 navbar-eventi"> 
                    <?php if(isset($templateParams["Tabella-Eventi"])){
                        require($templateParams["Tabella-Eventi"]);
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

        