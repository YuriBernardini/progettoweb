<div class="con">
    <div class="row">
        <div class="col-md rounded form-group mx-3 pb-3 event-form">
                <h2 class='my-4'>IL MIO CARRELLO</h2>
                <div class='row'>
                <div class='col-lg-12'>
                        <form action="acquista.php" method="POST" id="click-form-carrello" enctype="multipart/form-data">
                        <table class='table user-table table-hover'>
                            <thead class='thead-light'>
                                <tr>
                                    <th scope='col' hidden>Id</th>
                                    <th scope='col'>Titolo</th>
                                    <th scope='col' class='hide-column'>Data</th>
                                    <th scope='col'>Prezzo</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if(isset($_COOKIE[$_SESSION['idutente']])) {

                                foreach(explode(".", $_COOKIE[$_SESSION['idutente']]) as $eventi):                          
                                       
                                $n = explode("-", $eventi);

                                
                                $cercaEvento = $dbh->getEventByIdEvent($n[0]);
                                $evento = $cercaEvento[0];

                                $prezzo = $evento['prezzo'] * $n[1];
                                    
                                echo "
                                <tr class='row-table clickable-row-carrello'>
                                <td class='align-middle idevento' hidden>".$evento['id']."</td>
                                <td class='align-middle'>".$evento['titolo']."</td>
                                <td class='align-middle hide-column'>".$evento['data']."</td>
                                <td class='align-middle'>".$evento['prezzo']."&#8364; x ".$n[1]." = ".$prezzo." &#8364;</td>
                                </tr>";

                                endforeach;

                            echo "   
                            </tbody>
                            </table>
                            <button type='submit' name='reset' class='btn btn-carrello' id='btn-svuota-carrello' >Svuota carrello</button>
                            <button type='submit' name='submit-carrello' class='btn btn-carrello' id='btn-acquista-carrello' >Conferma acquisti</button>
                            <input type='hidden' name='idevento' value='' class='idevento-form-carrello'/>
                            </form>
                            </div>
                            </div>";
                        } else {
                            ?>
                        </tbody>
                    </table>
                    </form>
                </div>
            </div>
            <?php } ?>            
        </div>
    </div>
</div>