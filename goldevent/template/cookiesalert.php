<div class="alert text-center cookiealert" role="alert">
    <strong>Ti piacciono i cookies?</strong> &#x1F36A;Utilizziamo i cookie tecnici e analitici per assicurarti la migliore esperienza sul nostro sito Web. <a href="cookies.php" target="_blank">Per saperne di più</a>
        <button type="button" class="btn btn-primary btn-sm acceptcookies" aria-label="Close">
            Accetto
        </button>
    </div>