<?php
require_once("bootstrap.php");?>

<div id="pwdModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content border-0">
        <div class="modal-header">
            <h1 class="text-center">Richiesta nuova password</h1>
        </div>
        <div class="modal-body">
            <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="text-center">
                            <p>Se hai dimenticato la password, puoi reimpostarla qui inserendo la tua mail di registrazione.</p>
                            <div class="panel-body">
                                <form action="account.php" method="POST" id="recupera-pwd-form" enctype="multipart/form-data">
                                <fieldset>
                                    <div class="form-group">
                                        <label for="recuperaemail" class="col-lg-2 col-form-label" hidden>Email</label>
                                        <input class="form-control input-lg" placeholder="Inserisci Email" name="recuperaemail" type="email" id="recuperaemail" required>
                                    </div>
                                    <input class="btn" value="Recupera Password" type="submit">
                                </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="col-md-12">
                <button class="btn ml-3" data-dismiss="modal" aria-hidden="true">Annulla</button>
            </div>	
        </div>
    </div>
  </div>
</div>
