<div class="square p-3">
        <h2 class="text-uppercase">AREA PERSONALE DI <?php echo $_SESSION['nome']." ".$_SESSION['cognome'];  ?></h2>
</div>
<div class="container-md">
        <div class="row">
            <div class="col-md square m-3 p-2">
                    <a class="text-uppercase" href="biglietti.php">Visualizza i miei acquisti</a>
            </div>
            <div class="col-md square m-3 p-2 text-center">
                    <a class="text-uppercase" href="cruscotto-eventi.php">Cruscotto eventi</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md square m-3 mb-2 p-2">
                    <a class="text-uppercase" href="logout.php">ESCI</a>
            </div>
        </div>
</div>
    <div class="square p-2">
    <form action="cambia-dati.php" method="POST">
      <h3 class="text-uppercase p-3">modifica / gestisci i tuoi dati</h3>
      <fieldset>
        <legend>Cambia Password</legend>
      <div class="form-group row">
        <label for="old-psw" class="col-lg-2 col-form-label">Vecchia Password</label>
        <div class="col-lg-10">
          <input type="password" class="form-control" placeholder="Inserisci Vecchia Password" name="old-psw" id="old-psw" />
        </div>
      </div>
      <div class="form-group row">
        <label for="new-psw" class="col-lg-2 col-form-label">Nuova Password</label>
        <div class="col-lg-10">
          <input type="password" class="form-control" placeholder="Inserisci Nuova Password" name="new-psw" id="new-psw" />
        </div>
      </div>
      <div class="form-group row">
        <label for="psw-repeat" class="col-lg-2 col-form-label">Ripeti Password</label>
        <div class="col-lg-10">
          <input type="password" class="form-control" placeholder="Reinserisci Nuova Password" name="repeat-psw" id="psw-repeat" />
        </div>
      </div>
      </fieldset>
      <fieldset>
        <legend>Residenza</legend>
      <div class="form-group row">
        <label for="new-regione" class="col-lg-2 col-form-label">Regione</label>
        <div class="col-lg-10">
          <select class="form-control regioneselection" id="new-regione" name="new-regione">
          <option disabled selected value> -- seleziona un'opzione -- </option>
            <?php foreach($templateParams["Regioni"] as $regione): ?>
            <option value="<?php echo $regione['regione']; ?>"><?php echo $regione['regione']; ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label for="new-provincia" class="col-lg-2 col-form-label">Provincia</label>
        <div class="col-lg-10">
          <select class="form-control provinciaselection" id="new-provincia" name="new-provincia">
            <option disabled selected value> -- seleziona un'opzione -- </option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label for="new-citta" class="col-lg-2 col-form-label">Citt&agrave;</label>
        <div class="col-lg-10">
          <select class="form-control cittaselection" id="new-citta" name="new-citta">
            <option disabled selected value> -- seleziona un'opzione -- </option>
          </select>
        </div>
      </div>
      </fieldset>
      <div class="form-group row">
        <div class="col-lg">
          <button type="submit" class="btn m-3">AGGIORNA</button>
          <button type="button" class="btn m-3 px-4" data-toggle="modal" data-target="#exampleModal">
                                ELIMINA 
                                </button>
                                    <!-- Modal -->
                                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <h3 class="modal-title" id="exampleModalLabel">Vuoi davvero eliminare il tuo account?</h3>
                                        </div>
                                        <div class="modal-footer border-0">
                                          <a class="btn" href="disattiva-account.php">Elimina</a>    
                                          <button type="button" class="btn" data-dismiss="modal">Annulla</button>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                    <!-- fine Modal -->
        </div>
      </div>
      </form>
    </div>
<!-- <div class="container-md">
        <div class="row">
            <div class="col-md square mt-2 p-2">
                    <a class="text-uppercase" href="disattiva-account.php">ELIMINA IL MIO ACCOUNT</a>
            </div>
        </div>
</div> -->