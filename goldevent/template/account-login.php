<div class="con">
  <div class="row">
    <div class="col-md rounded form-group mx-3 login-form">
      <form action="account.php" method="POST">
      <h2 class="my-4 ">ACCEDI</h2>
      <div class="form-group row">
        <label for="email" class="col-lg-2 col-form-label">Email</label>
        <div class="col-lg-10">
          <input type="text" class="form-control" placeholder="Inserisci Email" name="email" id="email" required />
        </div>
      </div>
      <div class="form-group row">
        <label for="password" class="col-lg-2 col-form-label">Password</label>
        <div class="col-lg-10">
          <input type="password" class="form-control" placeholder="Inserisci Password" name="password" id="password" required />
        </div>
      </div>
      <div class="form-group row">
        <div class="col-lg">
          <button type="submit" class="btn">Accedi</button>
        </div>
      </div>
      <div class="container">
        <div class="col-lg">
          <span class="psw"><a href="#" data-target="#pwdModal" data-toggle="modal">Password Dimenticata ?</a></span>
        </div>
      </div>
      </form>
    </div>

    <div class="col-md rounded form-group mx-3 login-form">
      <form action="account.php" method="POST">
      <h2 class="my-4">REGISTRATI</h2>
      <div class="form-group row">
        <label for="nome" class="col-lg-2 col-form-label">Nome</label>
        <div class="col-lg-10">
          <input type="text" class="form-control" placeholder="Inserisci Nome" name="nome" id="nome" required />
        </div>
      </div>
      <div class="form-group row">
        <label for="cognome" class="col-lg-2 col-form-label">Cognome</label>
        <div class="col-lg-10">
          <input type="text" class="form-control" placeholder="Inserisci Cognome" name="cognome" id="cognome" required />
        </div>
      </div>
      <div class="form-group row">
        <label for="new-email" class="col-lg-2 col-form-label">Email</label>
        <div class="col-lg-10">
          <input type="text" class="form-control" placeholder="Inserisci Email" name="new-email" id="new-email" required />
        </div>
      </div>
      <div class="form-group row">
        <label for="new-psw" class="col-lg-2 col-form-label">Password</label>
        <div class="col-lg-10">
          <input type="password" class="form-control" placeholder="Inserisci Password" name="new-psw" id="new-psw" required />
        </div>
      </div>
      <div class="form-group row">
        <label for="repeat-psw" class="col-lg-2 col-form-label pt-0" >Ripeti Password</label>
        <div class="col-lg-10">
          <input type="password" class="form-control" placeholder="Conferma Password" name="repeat-psw" id="repeat-psw" required />
        </div>
      </div>
      <div class="form-group row">
        <label for="data" class="col-lg-2 col-form-label pt-0">Data di nascita</label>
        <div class="col-lg-10">
          <input type="datetime-local" name="data" id="data" class="form-control" />
        </div>
      </div>
      <fieldset>
        <legend>Residenza</legend>
        <div class="form-group row">
          <label for="regione" class="col-lg-2 col-form-label">Regione</label>
          <div class="col-lg-10">
            <select class="form-control regioneselection pointer" id="regione" name="regione">
              <option disabled selected value> -- seleziona un'opzione -- </option>
              <?php foreach($templateParams["Regioni"] as $regione): ?>
              <option value="<?php echo $regione['regione']; ?>"><?php echo $regione['regione']; ?></option>
              <?php endforeach; ?>
            </select>
          </div>
        </div>
        <div class="form-group row">
          <label for="provincia" class="col-lg-2 col-form-label">Provincia</label>
          <div class="col-lg-10">
            <select class="form-control provinciaselection pointer" id="provincia" name="provincia">
              <option disabled selected value> -- seleziona un'opzione -- </option>
            </select>
          </div>
        </div>
        <div class="form-group row">
          <label for="citta" class="col-lg-2 col-form-label">Citt&agrave;</label>
          <div class="col-lg-10">
            <select class="form-control cittaselection pointer" id="citta" name="citta">
              <option disabled selected value> -- seleziona un'opzione -- </option>
            </select>
          </div>
        </div>
        <div class="form-check">
          <input class="form-check-input checkbox pointer" type="checkbox" id="organizzatore" name="organizzatore" value="1">
          <label class="form-check-label pointer ml-2" for="organizzatore">
          Voglio essere un Organizzatore
          </label>
        </div>
      </fieldset>
      <div class="form-group row mt-3">
        <div class="col-lg">
          <button type="submit" class="btn">Registrati</button>
        </div>
      </div>
      </form>
    </div>
  </div>
</div>
<!--modal-->
<?php if(isset($templateParams["RecuperaPassword"])){
  include $templateParams["RecuperaPassword"];
}
?>
