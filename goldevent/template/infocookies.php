<article>
        <h2 class="py-4 text-uppercase">Cookies policy</h2>
    <section>
        <div class="intro-cookies text-left">
            <h3 class="pb-2">Stai leggendo la policy del servizio: Gold Event</h3>
            <p>In linea con la legislazione Europea, si vuole garantire che ogni utente del sito web comprenda cosa sono i cookie e per quale motivo vengono utilizzati, in modo che possa decidere consapevolmente se continuare ad utilizzare il sito o no.</p>
            <p>Un cookie è un piccolo file di testo contenente un numero di identificazione univoco che viene trasferito dal sito web sul disco rigido del computer attraverso un codice anonimo in grado di identificare il computer ma non l'utente e di monitorare passivamente le attività sul sito.</p>
            <p>I cookie possono essere classificati in quattro categorie, in base alle loro funzionalità:</p>
            <ul class="font-italic font-weight-bold">
                <li>Cookie strettamente necessari</li>
                <li>Cookie sulle prestazioni</li> 
                <li>Cookie funzionali</li>
                <li>Cookie di profilazione</li>
            </ul>
        </div>
        <div class="cookies-details text-left">
        <h3 class="pt-4 pb-2">Cookies utilizzati da Gold Event</h3>
        <p>Il sito web GoldEvent usa solo le prime tre categorie di cookies, per i quali non è richiesto alcun esplicito consenso.</p>
        <p><span class="font-italic font-weight-bold">Cookie strettamente necessari: </span>Questi cookie sono essenziali al fine di permettere la navigazione del sito web e l'utilizzo delle sue funzionalità, come ad esempio l'accesso ad alcune aree protette. Senza questi cookie, alcune funzionalità richieste come ad esempio il login al sito o la creazione di un carrello per lo shopping online non potrebbero essere fornite.</p>
        <p><span class="font-italic font-weight-bold">Cookie funzionali: </span>Questi cookie consentono al sito Web di ricordare le scelte che l'utente ha effettuato ( come il vostro lo username, la vostra lingua o l'area geografica in cui vivete vive l'utente) al fine di ottimizzare e fornire funzionalità più avanzate. Queste informazioni raccolte dai cookie possono essere anonime e non devono tracciare la navigazione e le attività dell'utente su altri siti web. Questi cookie possono anche essere definiti cookie tecnici.</p>
        <p><span class="font-italic font-weight-bold">Cookie analitici: </span>Questi cookie raccolgono informazioni su come gli utenti utilizzano il sito web, ad esempio quali pagine vengono visitate più spesso, e se gli utenti ricevono messaggi di errore da queste pagine. Questi cookie non raccolgono informazioni che identificano un visitatore specifico. Tutte le informazioni raccolte da questi cookie sono aggregate e quindi anonime. Vengono utilizzate unicamente per migliorare il funzionamento del sito web. Questi cookie possono essere paragonati ai "cookie tecnici" quando in alternativa (i) sono usati direttamente dall'amministratore del sito per raccogliere informazioni in forma aggregata oppure (ii) sono gestiti da terze parti e specifici mezzi sono adoperati per ridurre la capacità dei cookie di identificare gli utenti (per esempio, nascondendo una parte significativa dell'indirizzo IP) e ove la terza parte li utilizzi esclusivamente per la fornitura del servizio. Per esempio, esse archiviano i dati dei cookie separatamente senza incrociarli o arricchirli con altri dati personali disponibili.</p>
        <p>I cookie sopra citati sono definiti persistenti e la loro durata è stabilita dal server al momento della loro creazione.</p>
        </div>
        <div class="py-4 font-weight-bold text-left">
        <h4>Continuando la navigazione su questo sito, l'utente acconsente all'utilizzo dei cookie e dichiara di aver compreso la presente cookie policy.</h4>
        </div>
    </section>
</article>
