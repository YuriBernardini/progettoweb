
<div class="con">
    <div class="row">
        <div class="col-md rounded form-group mx-3 event-form">
            <h2 class="my-4">GESTISCI/CREA I TUOI EVENTI</h2>
            <div class="inseriscievento-btn">
            <form action="gestisci-eventi.php" method="POST" enctype="multipart/form-data">
                <input type="submit" class="btn inseriscievento-btn" id="inseriscievento" name="submit" value= "Inserisci evento" />
                <input type="hidden" name="action" value="1" class ="action"/>
            </form>
            </div>
            <div class="form-group row">
                 <?php if(isset($templateParams["Tabella-Eventi"])){
                     require($templateParams["Tabella-Eventi"]);
                    }
                 ?>
            </div>
        </div>
    </div>
</div>
    