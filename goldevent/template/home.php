<article>
    <header>
        <div class="img-fluid">
        <a href="eventi.php"><img src="./img/home.jpg" alt="collegamento lista eventi" class="img-fluid mt-3"/></a>
        </div>
        <h2>Scopri l'evento più adatto a te</h2>
    </header>
    <section>
        <p>Consulta i prossimi eventi disponibili con tanti filtri adatti a te, acquista i tuoi biglietti e preparati al divertimento</p>
    </section>
</article>
<form action="<?php echo $templateParams["Home-Evento"];?>" method="POST" id="click-form-home" enctype="multipart/form-data">
<div class="square my-3 p-3">
    <h3>Eventi in Primo Piano</h3>
    <div class="card-deck pt-3">
            <?php foreach($templateParams["EventiInPrimoPiano"] as $evento) :?>
            <div class="card clickable-random-event" id="<?php echo $evento["id"];?>">
                <img class="card-img-top cardImg pt-2" src="./img/<?php echo $evento['immagine'];?>" alt=" ">
                <div class="card-body">
                <h4 class="card-title"><?php echo $evento['titolo'];?></h4>
                <p class="card-text"><?php echo $evento['comune']." (".$evento['provincia'].") - ".$evento['regione'];?></p>
                </div>
                <div class="card-footer">
                <small class="text-muted"><?php echo $evento['data'];?></small>
                </div>
            </div>
            <?php
            endforeach;
            ?>
            <input type="hidden" name="idevento" value="" class ="idevento-random-form"/>
    </div>
</div>
</form>