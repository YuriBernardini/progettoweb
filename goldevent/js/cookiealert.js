$(document).ready(function(){

    "use strict";

    const cookieAlert = document.querySelector(".cookiealert");
    const acceptCookies = document.querySelector(".acceptcookies");

    if (!cookieAlert) {
       return;
    }

    cookieAlert.offsetHeight; // Forza il browser ad attivare il reflow

    // Mostra l'avviso se non si trova settato il cookie "accetta i cookie"
    if (!getCookie("acceptCookies")) {
        cookieAlert.classList.add("show");
    }

    // Quando si fa clic sul pulsante, viene un cookie con valitià di 1 anno per ricordare la scelta dell'utente e chiude il banner
    acceptCookies.addEventListener("click", function () {
        setCookie("acceptCookies", true, 365);
        cookieAlert.classList.remove("show");
    });

    // Funzionamento Cookie preso da w3schools
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
});
