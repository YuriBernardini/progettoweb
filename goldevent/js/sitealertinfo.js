$(document).ready(function(){

    "use strict";

    const siteAlert = document.querySelector(".sitealertinfo");
    const closeAlert = document.querySelector(".closealertinfo");
    let check = false;

    if (!siteAlert) {
       return;
    }

    siteAlert.offsetHeight; // Forza il browser ad attivare il reflow

    //se la condizione è falsa (c'è un errore) mostra il banner
    if (check == false) {
        siteAlert.classList.add("show");
    }

    // Quando si fa clic sul pulsante, viene settava la variabile a vera e chiuso il banner
    closeAlert.addEventListener("click", function () {
        if(check == false){
        check = true;
        siteAlert.classList.remove("show");
        }
    });
});
