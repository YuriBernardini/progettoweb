$(document).ready(function(){

    regioneSelection();
    provinciaSelection();
    resetfilter();

    $(".delete-event-button").click(function(){
        let idevento = $(".idevento").val();
        $.getJSON({
            url:"./api-php/apielimina.php",

            type:"POST",
            data:{
                action:"3",
                idevento:idevento,

            },
            success:function(data)
            {
                window.location.href = "./cruscotto-eventi.php?formmsg="+data;
            }
        })
    });

    //tabella eventi - riga cliccabile
    $(".clickable-row").click(function(){
        let $row = $(this).closest("tr");
        $(".idevento-form").val($row.find(".idevento").text());
        $("#click-form").submit();
    });

    //tabella eventi carrello - riga cliccabile
    $(document).on('click',".clickable-row-carrello",function () {
        $('#click-form-carrello').attr('action', 'acquista-eventi.php');
        let $row = $(this).closest("tr");
        $(".idevento-form-carrello").val($row.find(".idevento").text());
        $("#click-form-carrello").submit();
    });

    // //tabella eventi carrello - riga cliccabile
    // $(document).on('click','#btn-acquista-carrello',function () {
    //     $('#click-form-evento').attr('action', 'acquista.php');
    //     $("#click-form-evento").submit();
    // });

    //Home random eventi - div cliccabile
    $(".clickable-random-event").click(function(){
        let $id = $(this).attr('id');
        $(".idevento-random-form").val($id);
        $("#click-form-home").submit();
    });

    //Biglietti eventi - riga tabella cliccabile
    $(".clickable-biglietti-eventi").click(function(){
        let $id = $(this).attr('id');
        $(".idevento-biglietti-form").val($id);
        $("#click-form-biglietti").submit();
    });

    //admin disattiva evento
    $(document).on('click','.disattivaEvento',function () {
        var $row = $(this).closest("tr");    // Find the row
        // Find the text in data
        $.ajax({
            type : "POST",
            url : "./api-php/apidisattiva-evento.php",
            dataType: "json",
            data : {id: $row.find(".id-evento-attivato").text()},
            success : function(response) {
                let eventiDisattivati = stampaEventiNonAttivi(response);
                let eventiAttivati = stampaEventiAttivi(response);
                $(".eventiNonAttiviTbody").html(eventiDisattivati);
                $(".eventiAttiviTbody").html(eventiAttivati);
            },
        });
    });

     //admin attiva evento
     $(document).on('click','.attivaEvento',function () {
        var $row = $(this).closest("tr");    // Find the row
        // Find the text in data
        $.ajax({
            type : "POST",
            url : "./api-php/apiattiva-evento.php",
            dataType: "json",
            data : {id: $row.find(".id-evento-disattivato").text()},
            success : function(response) {
                let eventiDisattivati = stampaEventiNonAttivi(response);
                let eventiAttivati = stampaEventiAttivi(response);
                $(".eventiNonAttiviTbody").html(eventiDisattivati);
                $(".eventiAttiviTbody").html(eventiAttivati);
            },
        });
    });

     //admin disattiva utente
     $(document).on('click','.disattivaUtente',function () {
        var $row = $(this).closest("tr");    // Find the row
        // Find the text in data
        $.ajax({
            type : "POST",
            url : "./api-php/apidisattiva-utente.php",
            dataType: "json",
            data : {id: $row.find(".id-utente-attivato").text()},
            success : function(response) {
                let utentiDisattivati = stampaUtentiNonAttivi(response);
                let utentiAttivati = stampaUtentiAttivi(response);
                $(".utentiNonAttiviTbody").html(utentiDisattivati);
                $(".utentiAttiviTbody").html(utentiAttivati);
            },
        });
     });

     //admin attiva utente
     $(document).on('click','.attivaUtente',function () {
        var $row = $(this).closest("tr");    // Find the row
        // Find the text in data
        $.ajax({
            type : "POST",
            url : "./api-php/apiattiva-utente.php",
            dataType: "json",
            data : {id: $row.find(".id-utente-disattivato").text()},
            success : function(response) {
                let utentiDisattivati = stampaUtentiNonAttivi(response);
                let utentiAttivati = stampaUtentiAttivi(response);
                $(".utentiNonAttiviTbody").html(utentiDisattivati);
                $(".utentiAttiviTbody").html(utentiAttivati);
            },
        });
     });

});

//filtro Regioni - generatore elenco province regione selezionata
function regioneSelection(){
    $('.regioneselection').change(function(){
        let regione = $('.regioneselection :selected').val();
        if(regione != ""){
            $.getJSON({
                url:"./api-php/apicitta.php",
                type:"POST",
                data:{
                    regione:regione
                },
                success:function(data)
                {
                    let elenco = replaceProvincia(data);
                    $(".provinciaselection").html(elenco);
                }
            })
        }
   });
}

//filtro Provincia - generatore elenco citta della provincia selezionata
function provinciaSelection(){
    $('.provinciaselection').change(function(){
        let provincia = $('.provinciaselection :selected').val();
        if(provincia != ""){
            $.getJSON({
                url:"./api-php/apiprovincia.php",
                type:"POST",
                data:{
                    provincia:provincia
                },
                success:function(data)
                {
                    let elenco = replaceCitta(data);
                    $(".cittaselection").html(elenco);
                }
            })
        }
    });
}

//ricarica tutte le province
function replaceProvincia(provincia){
    result = `<option disabled selected value>-- seleziona un'opzione --</option>`;
    for(let i=0; i < provincia.length; i++){
        let elenco =`
            <option value="${provincia[i]["provincia"]}">${provincia[i]["provincia"]}</option>
        `;
        result += elenco;
    }
    return result;
}

//ricarica tutte le citta
function replaceCitta(citta){
    result = `<option disabled selected value>-- seleziona un'opzione --</option>`;
    for(let i=0; i < citta.length; i++){
        let elenco =`
            <option value="${citta[i]["comune"]}">${citta[i]["comune"]}</option>
        `;
        result += elenco;
    }
    return result;
}

//resetta il filtro e la ricerca della tabella
function resetfilter(){
    $(".btn-resetta-filtro").click(function(){
        $(".categoriaselection")[0].selectedIndex = 0;
        $(".regioneselection")[0].selectedIndex = 0;
        $(".provinciaselection")[0].selectedIndex = 0;
        $(".cittaselection")[0].selectedIndex = 0;
        $("#titoloevento").val("");
    
        let table, tr, i;
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
    
        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
            tr[i].style.display = "";
        }
    });
}

function filterTableCategoria() {
    // Declare variables
    let input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("categoria");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = $("tr:visible");
    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function filterTableRegione() {
    // Declare variables
    let input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("regione");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = $("tr:visible");
    // tr = table.getElementsByTagName("tr");
    
    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[6];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function filterTableProvincia() {
    // Declare variables
    let input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("provincia");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = $("tr:visible");
    
    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[7];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function filterTableCitta() {
    // Declare variables
    let input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("citta");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = $("tr:visible");
    
    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[8];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function filterTableTitolo() {
    // Declare variables
    let input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("titoloevento");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = $("tr:visible");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[2];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function stampaUtentiNonAttivi(utenti){
    let result="";
    let utenteInattivo = utenti["inattivi"];
    for(let i=0; i<utenteInattivo.length; i++){
        let utente= `
        <tr class='row-table attivaUtente'>
            <td class='align-middle hide-column id-utente-disattivato'>${utenteInattivo[i]["id"]}</td>
            <td class='align-middle hide-column'>${utenteInattivo[i]["cognome"]}</td>
            <td class='align-middle hide-column'>${utenteInattivo[i]["nome"]}</td>
            <td class='align-middle'>${utenteInattivo[i]["email"]}</td>
            <td class='align-middle'><button type='submit' class='btn' >Attiva</button></td>
        </tr>
        `;
        result += utente;
    }
    return result;
}

function stampaUtentiAttivi(utenti){
    let result="";
    let utenteAttivo = utenti["attivi"];
    for(let i=0; i<utenteAttivo.length; i++){
        let utente= `
        <tr class='row-table disattivaUtente'>
            <td class='align-middle hide-column id-utente-attivato'>${utenteAttivo[i]["id"]}</td>
            <td class='align-middle hide-column'>${utenteAttivo[i]["cognome"]}</td>
            <td class='align-middle hide-column'>${utenteAttivo[i]["nome"]}</td>
            <td class='align-middle'>${utenteAttivo[i]["email"]}</td>
            <td class='align-middle'><button type='submit' class='btn' >Disattiva</button></td>
        </tr>
        `;
        result += utente;
    }
    return result;
}


function stampaEventiNonAttivi(eventi){
    let result="";
    let eventiInattivo = eventi["inattivi"];
    for(let i=0; i<eventiInattivo.length; i++){
        let evento= `
        <tr class='row-table attivaEvento'>
            <td class='align-middle id-evento-disattivato'>${eventiInattivo[i]["id"]}</td>
            <td class='align-middle'>${eventiInattivo[i]["titolo"]}</td>
            <td class='align-middle hide-column'>${eventiInattivo[i]["data"]}</td>
            <td class='align-middle'><button type='submit' class='btn' >Attiva</button></td>
        </tr>
        `;
        result += evento;
    }
    return result;
}

function stampaEventiAttivi(eventi){
    let result="";
    let eventiAttivo = eventi["attivi"];
    for(let i=0; i<eventiAttivo.length; i++){
        let evento= `
        <tr class='row-table disattivaEvento'>
            <td class='align-middle id-evento-attivato'>${eventiAttivo[i]["id"]}</td>
            <td class='align-middle'>${eventiAttivo[i]["titolo"]}</td>
            <td class='align-middle hide-column'>${eventiAttivo[i]["data"]}</td>
            <td class='align-middle'><button type='submit' class='btn' >Disattiva</button></td>
        </tr>
        `;
        result += evento;
    }
    return result;
}