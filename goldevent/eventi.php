<?php
require_once 'bootstrap.php';

//Base Template
$templateParams['Titolo'] = "GoldEvent - Eventi";
$templateParams['Icona'] = "img/icona.png";
$templateParams["Nome"] = "lista-eventi.php";
$templateParams["Home-Evento"] = "acquista-eventi.php";
$templateParams["Regioni"] = $dbh->getRegioni();
$templateParams["Categoria"] = $dbh->getCategorie();
$templateParams["Tabella-Eventi"] = "tabella-eventi.php";
$templateParams["Eventi"] = $dbh->getAllEventiNonSuperati();

if(isset($_GET["formmsg"])){

    $templateParams["AllarmeInfo"] = $_GET["formmsg"];
}

if(isset($_SESSION['acquistato'])) {

    $templateParams["AllarmeInfo"] = $_SESSION['acquistato'];
    unset($_SESSION['acquistato']);
}

if(isset($_SESSION['errore'])) {

    $templateParams["Allarme"] = $_SESSION['errore'];
    unset($_SESSION['errore']);
}
if(isset($_SESSION['AllarmeInfo'])) {

    $templateParams["AllarmeInfo"] = $_SESSION['AllarmeInfo'];
    unset($_SESSION['AllarmeInfo']);
}

require 'template/base.php';

?>