<?php
require_once 'bootstrap.php';

//Base Template
if(isUserLoggedIn()){

    if($_SESSION['privilegio'] != 3) {
        
        $templateParams['Titolo'] = "GoldEvent - Cruscotto eventi";
        $templateParams['Icona'] = "img/icona.png";
        $templateParams["Nome"] = "cruscotto-home.php";
        $templateParams["Tabella-Eventi"] = "tabella-eventi.php";
        $templateParams["Home-Evento"] = "gestisci-eventi.php";
        $templateParams["Azione"] = "2";
        $templateParams["Eventi"] = $dbh->getUserEvents($_SESSION["idutente"]);
        
        if(isset($_GET["formmsg"])){
            $templateParams["AllarmeInfo"] = $_GET["formmsg"];
        }

    } else {
        header("location: eventi.php");
    }
}else{
    header("location: account.php");
}
require 'template/base.php';

?>