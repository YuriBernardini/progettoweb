<?php
require_once("bootstrap.php");

//Base Template
$templateParams['Titolo'] = "GoldEvent - Acquista";
$templateParams['Icona'] = "img/icona.png";
$templateParams["Nome"] = "account.php";

if(!isset($_COOKIE[$_SESSION['idutente']])) {

    header('location:account.php');
    
} else {

    if(isset($_POST['reset'])) {

        unset($_COOKIE[$_SESSION['idutente']]);
        setcookie($_SESSION['idutente'], "", time() - 3600, '/');
        
        header('location:carrello.php');

    } else if (isset($_POST['submit-carrello'])){
        
        foreach(explode(".", $_COOKIE[$_SESSION['idutente']]) as $eventi):                          
                                        
            $n = explode("-", $eventi);

            $cercaEvento = $dbh->getEventByIdEvent($n[0]);

            if(count($cercaEvento)!=0) {

                $evento = $cercaEvento[0];

                //controllare numero disponibile
                if($evento['bigliettiDisponibili'] <  $n[1]) {
                    
                    $templateParams['Allarme'] = "Biglietti Terminati per ".$evento['titolo'];

                } else {
                    
                    $prezzo = $evento['prezzo'] * $n[1];

                    $login_result = $dbh->getBigliettoByIds($_SESSION['idutente'], $evento['id']);

                    if(count($login_result)==0){
                        //nuovo acquisto
                        $dbh->buyFirstTicket($_SESSION['idutente'], $evento['id'], $n[1], $prezzo);

                        $templateParams["AllarmeInfo"] = "Acquisto effettuato correttamente";
                    }
                    else{
                        //si tratta di ulteriori biglietti poichè gia acquistato
                        $dbh->buyOtherTicket($_SESSION['idutente'], $evento['id'], $n[1], $prezzo);

                        $templateParams["AllarmeInfo"] = "Acquisto effettuato correttamente";
                    }
                }
            }

        endforeach;

        unset($_COOKIE[$_SESSION['idutente']]);
        setcookie($_SESSION['idutente'], "", time() - 3600, '/');
    }
}

require 'template/base.php';

?>