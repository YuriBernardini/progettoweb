<?php

function isActive($pagename) {
    if(basename($_SERVER['PHP_SELF']) == $pagename) {
        echo " class = 'nav-item active' ";
    }
    else {
        echo " class = 'nav-item' ";
    }
}

function isActiveAdmin($pagename) {
    if(basename($_SERVER['PHP_SELF']) == $pagename) {
        echo    "<li class = 'nav-item active'>
                    <a class='nav-link' href='admin.php'>ADMIN</a>
                </li>";
    }
    else {
        echo    "<li class = 'nav-item'>
                 <a class='nav-link' href='admin.php'>ADMIN</a>
                </li>";
    }
}

function setCookieCarrello($idutente, $idevento, $numero) {

    $cookie_name = $idutente;

    if(isset($_COOKIE[$cookie_name])) {

        foreach(explode(".", $_COOKIE[$cookie_name]) as $eventi):                          
                                        
            $n = explode("-", $eventi);

            //se id uguale a questo che scorro
            if($n[0] == $idevento) {

                $totale_biglietti = $n[1] + $numero;

                if($cookie_value == "") {
                    $cookie_value = $n[0]."-".$totale_biglietti;
                } else {
                    $cookie_value = $cookie_value.".".$n[0]."-".$totale_biglietti;
                }

                $ok = 1;

            } else {

                if($cookie_value == "") {
                    $cookie_value = $n[0]."-".$n[1];
                } else {
                    $cookie_value = $cookie_value.".".$n[0]."-".$n[1];
                }
                
            }

        endforeach;

        if(!isset($ok)) {
            //vecchio
            $cookie_value = $_COOKIE[$cookie_name].".".$idevento."-".$numero;
        } else {
            unset($ok);
        }

        setcookie($cookie_name, $cookie_value, time() + (86400), "/");

    } else {

        $cookie_value = $idevento."-".$numero;
        setcookie($cookie_name, $cookie_value, time() + (86400), "/"); // 86400 = 1 day
    }
}

function isUserLoggedIn(){
    return !empty($_SESSION['idutente']);
}

function getPrivilegio(){
    return !empty($_SESSION['privilegio']);
}

function registerLoggedUser($user){
    $_SESSION["idutente"] = $user["id"];
    $_SESSION["privilegio"] = $user["privilegio"];
    $_SESSION["nome"] = $user["nome"];
    $_SESSION["cognome"] = $user["cognome"];
    $_SESSION["email"] = $user["email"];
    $_SESSION["citta"] = $user["citta"];
}

function logout(){
    session_destroy();
    /*unset($_SESSION["idutente"]);*/

    header('location:account.php');
}

function emailOk($email) {
    //se non è una mail valida
    if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
        return false;
    }
    
    return true;
}

function pwdOk($pwd1, $pwd2) {
    
    if($pwd1 == $pwd2) {
        return true;
    }

    return false;
}

function dataOk($data) {

    $ok = date('Y-m-d', strtotime('-14 years'));

    if($ok > $data) {

        return true;

    }
    return false;
}

function getAction($action){
    $result = "";
    switch($action){
        case 1:
            $result = "Inserisci";
            break;
        case 2:
            $result = "Modifica";
            break;
    }

    return $result;
}

function getEmptyEvent(){
    return array("titolo" => "", "informazioni" => "", "immagine" => "", "bigliettiDisponibili" => "", "prezzo" => "", "citta" => "", "tipologia" => array());
}

function uploadImage($path, $image){
    $imageName = basename($image["name"]);
    $fullPath = $path.$imageName;
    
    $maxKB = 2500;
    $acceptedExtensions = array("jpg", "jpeg", "png", "gif");
    $result = 0;
    $msg = "";

    //Controllo se immagine è veramente un'immagine
    $imageSize = getimagesize($image["tmp_name"]);
    if($imageSize === false) {
        $msg .= "File caricato non è un'immagine! ";
    }
    //Controllo dimensione dell'immagine < 500KB
    if ($image["size"] > $maxKB * 1024) {
        $msg .= "File caricato pesa troppo! Dimensione massima è $maxKB KB. ";
    }

    //Controllo estensione del file
    $imageFileType = strtolower(pathinfo($fullPath,PATHINFO_EXTENSION));
    if(!in_array($imageFileType, $acceptedExtensions)){
        $msg .= "Accettate solo le seguenti estensioni: ".implode(",", $acceptedExtensions);
    }

    //Controllo se esiste file con stesso nome ed eventualmente lo rinomino
    if (file_exists($fullPath)) {
        $i = 1;
        do{
            $i++;
            $imageName = pathinfo(basename($image["name"]), PATHINFO_FILENAME)."_$i.".$imageFileType;
        }
        while(file_exists($path.$imageName));
        $fullPath = $path.$imageName;
    }

    //Se non ci sono errori, sposto il file dalla posizione temporanea alla cartella di destinazione
    if(strlen($msg)==0){
        if(!move_uploaded_file($image["tmp_name"], $fullPath)){
            $msg.= "Errore nel caricamento dell'immagine.";
        }
        else{
            $result = 1;
            $msg = $imageName;
        }
    }
    return array($result, $msg);
}

?>