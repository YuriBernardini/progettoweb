<?php
require_once 'bootstrap.php';

//Base Template
$templateParams['Titolo'] = "GoldEvent - Gestisci-Eventi";
$templateParams['Icona'] = "img/icona.png";

if(isset($_POST["action"]) && isUserLoggedIn()){
    $templateParams["Nome"] = "eventi-form.php";
    if($_POST["action"]!=1){
        $risultato = $dbh->getEventByIdEventAndIdUser($_POST["idevento"], $_SESSION["idutente"]);
        if(count($risultato)==0){
            $templateParams["Evento"] = null;
        }
        else{
            $templateParams["Evento"] = $risultato[0];
            $templateParams["CittaSelezionata"] = $dbh->getCittaSelezionata($risultato[0]["citta"]);
        }
    }
    else{
        $templateParams["Evento"] = getEmptyEvent();
    }
}
else{
    header("location: account.php");
}

$templateParams["Categoria"] = $dbh->getCategorie();
$templateParams["Provincia"] = $dbh->getProvince();
$templateParams["Citta"] = $dbh->getCitta();
$templateParams["Regioni"] = $dbh->getRegioni();

$templateParams["Azione"] = $_POST["action"];

require 'template/base.php';

?>
   