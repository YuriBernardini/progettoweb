<?php
require_once("bootstrap.php");

//Base Template
$templateParams['Titolo'] = "GoldEvent - Biglietti";
$templateParams['Icona'] = "img/icona.png";
$templateParams["Nome"] = "biglietti.php";
$templateParams["Home-Evento"] = "acquista-eventi.php";

if(!isset($_SESSION['idutente'])) {

    header('location:account.php');
    
} else {
    $templateParams["Biglietti"] = $dbh->getBiglietti($_SESSION['idutente']);
}

require 'template/base.php';
?>