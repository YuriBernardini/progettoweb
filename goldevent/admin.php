<?php

require_once("bootstrap.php");

if(isset($_SESSION['idutente']) && $_SESSION['privilegio'] == 1) {
    //Base Template
    $templateParams['Titolo'] = "GoldEvent - Admin";
    $templateParams['Icona'] = "img/icona.png";
    $templateParams["Nome"] = "admin.php";
    
    require 'template/base.php';
} else {
   header('location:index.php');
}

?>