<?php
require_once 'bootstrap.php';

if(!isUserLoggedIn() || !isset($_POST["action"])){
    header("location, account.php");
}

//inserimento
if(isset($_POST["action"]) && $_POST["action"] == 1 && isset($_SESSION["idutente"]) && $_SESSION["privilegio"] != 3){
    $titoloevento = $_POST["titoloevento"];
    $descrizioneevento = $_POST["descrizioneevento"];
    $bigliettidisponibili = $_POST["bigliettidisponibili"];
    $prezzoBiglietto = $_POST["prezzobiglietto"];
    $data = date("Y-m-d H:i:s", strtotime($_POST["dataevento"]));
    $regione = $_POST["regione"];
    $provincia = $_POST["provincia"];
    $citta = $_POST["citta"];
    $utente = $_SESSION["idutente"];
    $categoria = $_POST["categoria"];

    list($result, $msg) = uploadImage(UPLOAD_DIR, $_FILES["immagine"]);
    if($result != 0){
        $locandinaevento = $msg;
        $id = $dbh->insertEvent($titoloevento, $data, $descrizioneevento, $locandinaevento, $bigliettidisponibili, $prezzoBiglietto, $regione, $provincia, $citta, $utente, $categoria);
        if($id != false){
            $msg = "Inserimento effettuato correttamente e in attesa di approvazione!";
        }
        else{
        $msg = "Errore in inserimento!";
        }
    }
    $templateParams["AllarmeInfo"] = $msg;
    header("location: cruscotto-eventi.php?formmsg=".$msg);
}


//modifica
if(isset($_POST["action"]) && $_POST["action"] == 2 && $_SESSION["idutente"] == $_POST["idutente"] && $_SESSION["privilegio"] != 3){
    $idevento = $_POST["idevento"];
    $titoloevento = $_POST["titoloevento"];
    $descrizioneevento = $_POST["descrizioneevento"];
    $bigliettidisponibili = $_POST["bigliettidisponibili"];
    $prezzoBiglietto = $_POST["prezzobiglietto"];
    $regione = $_POST["regione"];
    $provincia = $_POST["provincia"];
    $citta = $_POST["citta"];
    $utente = $_SESSION["idutente"];
    $categoria = $_POST["categoria"];
    $data = date("Y-m-d H:i:s", strtotime($_POST["dataevento"]));

    if(isset($_FILES["immagine"]) && strlen($_FILES["immagine"]["name"]) >0){
        list($result, $msg) = uploadImage(UPLOAD_DIR, $_FILES["immagine"]);
        if($result == 0){
            header("location, account.php?formsg=".$msg);
        }
        $locandinaevento = $msg;
    }
    else{
        $locandinaevento = $_POST["locandinavecchia"];
    }
    $dbh->updateEventByCreator($idevento, $titoloevento, $data, $descrizioneevento, $locandinaevento, $bigliettidisponibili, $prezzoBiglietto, $regione, $provincia, $citta, $utente, $categoria);
    $msg = "Modifica effettuata correttamente!";
    $templateParams["AllarmeInfo"] = $msg;
    header("location: cruscotto-eventi.php?formmsg=".$msg);
}

require 'template/base.php';
?>